CREATE VIEW ManageEmployee AS
SELECT EmployeeID,EmployeeName,EmployeeSurname,DeptName,ManagerName,ManagerSurName
FROM Employee LEFT JOIN Department  ON (Employee.DeptID = Department.DeptID)
   LEFT JOIN Manager ON (Department.DeptID = Manager.DeptID )
