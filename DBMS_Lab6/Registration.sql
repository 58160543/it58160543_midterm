CREATE VIEW Registration AS 

	SELECT MS.ScheduleID , MS.ScheduleName,
		M.MovieName , C.CinemaName,
		MS.TimeSchedule

	FROM MovieSchedules AS MS
	LEFT JOIN (Movies AS M, Cinema AS C)
	ON 	
	MS.MovieID=M.MovieID and 
	MS.CinemaID = C.CinemaID
