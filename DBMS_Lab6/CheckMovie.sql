DROP PROCEDURE IF EXISTS CheckMovie;

DELIMITER $$
CREATE PROCEDURE CheckMovie
(IN RegID VARCHAR(10),OUT CustName VARCHAR(50),OUT CustSurname VARCHAR(50))
BEGIN

	SELECT CustomerName , CustomerSurname
	INTO CustName , CustSurname
	FROM Regist
	WHERE 
		RegistID = RegID;

END $$
DELIMITER ;
