DROP PROCEDURE IF EXISTS CheckSchedule;

DELIMITER $$

CREATE  PROCEDURE CheckSchedule
(IN SchID VARCHAR(10),IN MvID VARCHAR(10),
OUT CinID VARCHAR(10),OUT TimeSch DateTime)
BEGIN

	SELECT CinemaID , TimeSchedule

	INTO CinID , TimeSch 
	FROM MovieSchedules
	WHERE 
		ScheduleID = SchID and
		MovieID = MvID;


END $$
DELIMITER ;

