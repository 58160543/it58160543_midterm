Create Table MovieSchedules
(
	ScheduleID	VARCHAR(10)	NOT NULL,
	ScheduleName	VARCHAR(50)	NOT NULL,
	MovieID		VARCHAR(10)	NOT NULL,
	CinemaID	VARCHAR(10)	NOT NULL,
	TimeSchedule	DATETIME	NOT NULL,
	PRIMARY KEY(ScheduleID),
	FOREIGN KEY(MovieID) REFERENCES Movies(MovieID),
	FOREIGN KEY(CinemaID) REFERENCES Cinema(CinemaID)

) Engine=InnoDB;
